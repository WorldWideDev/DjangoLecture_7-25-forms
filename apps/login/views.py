# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponseRedirect, reverse
from .forms import FunForm
from .models import User
# Create your views here.
def index(req):
    print User.objects.all()
    # unbound
    form = FunForm()


    context = {
        "form": form
    }
    return render(req, 'login/index.html', context)

def register(req):
    bound = FunForm(req.POST)
    # return redirect('/')
    if bound.is_valid():
        bound.save()
        return HttpResponseRedirect(reverse('login:index'))
    context = {
        "form": bound
    }
    return render(req, 'login/index.html', context) 