# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=100, unique=True)
    bio = models.TextField()
    password = models.CharField(max_length=100)