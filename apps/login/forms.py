from django import forms
from .models import User
class FunForm(forms.ModelForm):

    name = forms.CharField(
        label="Your Name",
        min_length=3
    )
    bio = forms.CharField(
        required = False,
        max_length=20,
        label="Bio Here",
        widget=forms.Textarea(
            attrs={
                "rows": 5,
                "cols": 30
            }
        )
    )
    password = forms.CharField(
        widget = forms.PasswordInput()
    )
    password_confirm = forms.CharField(
        widget = forms.PasswordInput()
    )

    class Meta:
        model = User
        fields = (
            'name',
            'bio',
            'password',
            'password_confirm'
        )


    def clean(self):
        cleaned = self.cleaned_data
        if len(User.objects.filter(name=cleaned['name'])) > 0:
            raise forms.ValidationError("name exists")
        if cleaned['password'] != cleaned['password_confirm']:
            raise forms.ValidationError("Passwords do not match")